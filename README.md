## Game integration service

An absolute path must be specified for the logger.

You can run the service using any artifact name. For example: localhost/any_artifact_name/game/bet


### Project structure:

- domain.mapper.*: Сontains classes that map bets, wins, and balance request and response bodies from web layer to domain layer.
- domain.model.*: Сontains models that are used as bets, wins, and balance request and response bodies on domain layer.
- domain.model.RequestHandler class: Responsible for storing Bet and Win data by Game Id in memory to check the correct sequence of bets and wins.
- domain.service.impl.BetValidationServiceImpl class: implements BetValidationService. Responsible for validation bets in the process of Game Flow validation.
- domain.service.impl.WinValidationServiceImpl class: implements WinValidationService. Responsible for validation wins in the process of Game Flow validation.

- web.api.*: interfaces and classes responsible for communication with Simple Wallet.
- web.api.connection.ConnectionFactory class: Responsible for creation connections to Simple Wallet.
- web.api.impl.AbstractGenericWalletApiRequest class: Abstract class. Responsible for communication with Simple Wallet.
- web.api.impl.BalanceWalletApiRequestImpl class: extends AbstractGenericWalletApiRequest implements BalanceWalletApiRequest. Responsible for communication with Simple Wallet, serving balance GET requests.
- web.api.impl.BetWalletApiRequestImpl class: extends AbstractGenericWalletApiRequest implements BetWalletApiRequest. Responsible for communication with Simple Wallet, serving bet POST requests.
- web.api.impl.WinWalletApiRequestImpl class: extends AbstractGenericWalletApiRequest implements WinWalletApiRequest. Responsible for communication with Simple Wallet, serving win POST requests.
- web.api.util.WalletApiRequestExceptionUtil class: Responsible for checking if the response from Simple Wallet has an "errors" status.
- web.context.ApplicationContextInjector class: Responsible for instances initializing.
- web.controller.filter.wrapper.RequestWrapper class: Used in RequestHashFilter class to correctly hash validation of requests.
- web.controller.filter.RequestHashFilter class: the Filter class. Serves Hash validation of requests.
- web.controller.BalanceServlet class: the servlet class. Serves balance GET requests.
- web.controller.BetServlet class: the servlet class. Serves bet POST requests.
- web.controller.WinServlet class: the servlet class. Serves win POST requests.
- web.exceptions.model.* : Сontains models that are used as error-responses bodies.
- web.exceptions.service.impl.ErrorResponseServiceImpl class: implements ErrorResponseService. Responsible for mapping exceptions as error-response models.
- web.exceptions.ErrorCodeConstant interface: Error Code constants list.
- web.exceptions.ExceptionCode enum: Error messages list according to Error Code constants.
- web.exceptions.WebException class: Custom "exception wrapper" to handle exceptions. 
- web.model.enams.ResponseStatus enum: Lists possible response statuses.
- web.model.* : Сontains models that are used as bets, wins, and balance request and response bodies on web layer.
- web.service.impl.BalanceRequestHashValidationImpl class: implements RequestHashValidation. Responsible for balance requests hash validation.
- web.service.impl.BetAndWinRequestHashValidation class: implements RequestHashValidation. Responsible for bet and win requests hash validation.
- web.service.SecretSaltFactory class: Responsible for getting secretSalt value from properties.

- resources
