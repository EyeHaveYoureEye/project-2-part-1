package main.java.tech.pm.edu.gameintegrationservice.domain.service.impl;

import org.apache.log4j.Logger;
import tech.pm.edu.gameintegrationservice.domain.mapper.*;
import tech.pm.edu.gameintegrationservice.domain.model.BetRequestDomain;
import tech.pm.edu.gameintegrationservice.domain.model.RequestHandler;
import tech.pm.edu.gameintegrationservice.domain.model.WinRequestDomain;
import tech.pm.edu.gameintegrationservice.domain.service.WinValidationService;
import tech.pm.edu.gameintegrationservice.web.exceptions.ExceptionCode;
import tech.pm.edu.gameintegrationservice.web.exceptions.WebException;
import tech.pm.edu.gameintegrationservice.web.model.WinRequest;

import java.util.Queue;

public class WinValidationServiceImpl implements WinValidationService {
  private static final Logger LOG = Logger.getLogger(WinValidationServiceImpl.class);

  private final RequestHandler requestHandler = RequestHandler.getInstance();

  @Override
  public void gameValidation(WinRequest winRequest) {
    WinRequestDomain winRequestDomain = WinRequestMapper.winRequestToWinRequestDomain(winRequest);
    if (winRequestDomain.getAmount().doubleValue() <= 0) {
      LOG.error("Unexpected error: win request for playerId: " + winRequest.getPlayerId() + " can't be processed due to negative amount ");
      throw new WebException(ExceptionCode.UNEXPECTED_ERROR);
    } else if (!requestHandler.isGameExistsInCache(winRequestDomain.getTransaction().getGameId())) {
      LOG.error("Unexpected error: gameId: " + winRequest.getTransaction().getGameId() + " doesn't exists in the cache");
    } else if (!isBetForCurrentWinExists(winRequestDomain)) {
      LOG.error("Bet with specified id not found in the cache");
    } else if (!isTxIdUnique(winRequestDomain)) {
      LOG.error("Unexpected error: game bet and win has same transaction ID");
      throw new WebException(ExceptionCode.UNEXPECTED_ERROR);
    }
  }

  @Override
  public void addWinRequest(WinRequest winRequest) {
    requestHandler.addGameWin(WinRequestMapper.winRequestToWinRequestDomain(winRequest));
    requestHandler.checkBetsQueueSize(WinRequestMapper.winRequestToWinRequestDomain(winRequest).getTransaction().getGameId());
  }

  private boolean isBetForCurrentWinExists(WinRequestDomain winRequestDomain) {
    Queue<BetRequestDomain> betsForGame = requestHandler.getBetRequestQueueByGameId(winRequestDomain.getTransaction().getGameId());
    for (BetRequestDomain betRequestDomain : betsForGame) {
      if (betRequestDomain.getPlayerId().equals(winRequestDomain.getPlayerId()) &&
        betRequestDomain.getTransaction().getRoundId().equals(winRequestDomain.getTransaction().getRoundId()) &&
        betRequestDomain.getTransaction().getGameId().equals(winRequestDomain.getTransaction().getGameId())) {
        return true;
      }
    }
    return false;
  }

  private boolean isTxIdUnique(WinRequestDomain winRequest) {
    Queue<BetRequestDomain> betsForGame = requestHandler.getBetRequestQueueByGameId(winRequest.getTransaction().getGameId());
    Queue<WinRequestDomain> winsForGame = requestHandler.getWinRequestQueueByGameId(winRequest.getTransaction().getGameId());
    for (BetRequestDomain betRequestDomain : betsForGame) {
      if (betRequestDomain.getTransaction().getTxId().equals(winRequest.getTransaction().getTxId()) &&
        betRequestDomain.getTransaction().getRoundId().equals(winRequest.getTransaction().getRoundId()) &&
        betRequestDomain.getPlayerId().equals(winRequest.getPlayerId())) {
        return false;
      }
    }
    for (WinRequestDomain winRequestDomain : winsForGame) {
      if (winRequestDomain.getTransaction().getTxId().equals(winRequest.getTransaction().getTxId()) &&
        winRequestDomain.getTransaction().getRoundId().equals(winRequest.getTransaction().getRoundId()) &&
        winRequestDomain.getPlayerId().equals(winRequest.getPlayerId())) {
        return false;
      }
    }
    return true;
  }


}
