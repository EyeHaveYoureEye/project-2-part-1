package main.java.tech.pm.edu.gameintegrationservice.domain.service.impl;

import org.apache.log4j.Logger;
import tech.pm.edu.gameintegrationservice.domain.mapper.BetRequestMapper;
import tech.pm.edu.gameintegrationservice.domain.model.BetRequestDomain;
import tech.pm.edu.gameintegrationservice.domain.model.RequestHandler;
import tech.pm.edu.gameintegrationservice.domain.service.BetValidationService;
import tech.pm.edu.gameintegrationservice.web.exceptions.ExceptionCode;
import tech.pm.edu.gameintegrationservice.web.exceptions.WebException;
import tech.pm.edu.gameintegrationservice.web.model.BetRequest;

public class BetValidationServiceImpl implements BetValidationService {
  private static final Logger LOG = Logger.getLogger(BetValidationServiceImpl.class);

  private final RequestHandler requestHandler = RequestHandler.getInstance();

  @Override
  public void gameValidation(BetRequest betRequest) {
    if (betRequest.getAmount().doubleValue() <= 0) {
      LOG.error("Bet request for playerId: " + betRequest.getPlayerId() + " can't be processed due to negative amount ");
      throw new WebException(ExceptionCode.UNEXPECTED_ERROR);
    } else if (isBetForCurrentRoundExists(BetRequestMapper.betRequestToBetRequestDomain(betRequest))) {
      LOG.error(String.format("Bet already settled for playerId: %s, gameId: %s, roundId: %s",
        betRequest.getPlayerId(), betRequest.getTransaction().getGameId(), betRequest.getTransaction().getRoundId()));
      throw new WebException(ExceptionCode.BET_ALREADY_SETTLED);
    };
  }

  @Override
  public void addGameBet(BetRequest betRequest) {
    requestHandler.addGameBet(BetRequestMapper.betRequestToBetRequestDomain(betRequest));
    requestHandler.checkBetsQueueSize(BetRequestMapper.betRequestToBetRequestDomain(betRequest).getTransaction().getGameId());
  }

  private boolean isBetForCurrentRoundExists(BetRequestDomain betRequestDomain) {
    if (requestHandler.getBetRequestQueueByGameId(betRequestDomain.getTransaction().getGameId()) != null) {
      for (BetRequestDomain requestDomain : requestHandler.getBetRequestQueueByGameId(betRequestDomain.getTransaction().getGameId())) {
        if (requestDomain.getPlayerId().equals(betRequestDomain.getPlayerId()) &&
          requestDomain.getTransaction().getRoundId().equals(betRequestDomain.getTransaction().getRoundId()) &&
          requestDomain.getTransaction().getGameId().equals(betRequestDomain.getTransaction().getGameId())) {
          return true;
        }

      }
    }
    return false;
  }


}
