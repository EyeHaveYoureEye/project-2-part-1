package main.java.tech.pm.edu.gameintegrationservice.domain.mapper;


import tech.pm.edu.gameintegrationservice.domain.model.WinRequestDomain;
import tech.pm.edu.gameintegrationservice.web.model.WinRequest;

public class WinRequestMapper {

  private WinRequestMapper() {
  }

  public static WinRequestDomain winRequestToWinRequestDomain(WinRequest winRequest) {
    if (winRequest == null) {
      return null;
    }
    return new WinRequestDomain(
      winRequest.getPlayerId(),
      winRequest.getAmount(),
      winRequest.getCurrency(),
      WinTransactionRequestMapper.winTransactionRequestToWinTransactionRequestDomain(winRequest.getTransaction())
    );
  }


}
