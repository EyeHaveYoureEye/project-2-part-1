package main.java.tech.pm.edu.gameintegrationservice.domain.mapper;

import tech.pm.edu.gameintegrationservice.domain.model.BetRequestDomain;
import tech.pm.edu.gameintegrationservice.web.model.BetRequest;

public class BetRequestMapper {

  private BetRequestMapper() {
  }

  public static BetRequestDomain betRequestToBetRequestDomain(BetRequest betRequest) {
    if (betRequest == null) {
      return null;
    }
    return new BetRequestDomain(
      betRequest.getPlayerId(),
      betRequest.getAmount(),
      betRequest.getCurrency(),
      BetTransactionRequestMapper.betTransactionRequestToBetTransactionRequestDomain(betRequest.getTransaction())
    );
  }


}
