package main.java.tech.pm.edu.gameintegrationservice.domain.model;

import lombok.Value;

import java.math.BigDecimal;

@Value
public class WinRequestDomain {

  Integer playerId;
  BigDecimal amount;
  String currency;
  WinTransactionRequestDomain transaction;


}
