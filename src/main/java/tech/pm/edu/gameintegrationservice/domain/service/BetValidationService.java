package main.java.tech.pm.edu.gameintegrationservice.domain.service;

import tech.pm.edu.gameintegrationservice.web.model.BetRequest;

public interface BetValidationService {

  void gameValidation(BetRequest betRequest);

  void addGameBet(BetRequest betRequest);


}
