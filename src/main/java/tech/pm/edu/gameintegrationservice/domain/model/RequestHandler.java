package main.java.tech.pm.edu.gameintegrationservice.domain.model;

import lombok.Getter;
import org.apache.log4j.Logger;

import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ConcurrentMap;

public class RequestHandler {

  private static final RequestHandler INSTANCE = new RequestHandler();

  @Getter
  private final ConcurrentMap<String, Queue<BetRequestDomain>> bets = new ConcurrentHashMap<>();
  private final ConcurrentMap<String, Queue<WinRequestDomain>> wins = new ConcurrentHashMap<>();

  private RequestHandler() {
  }

  public void checkBetsQueueSize(String gameId) {
    Queue<BetRequestDomain> betRequestDomainQueue = bets.get(gameId);
    if (betRequestDomainQueue.size() >= 1000) {
      betRequestDomainQueue.poll();
    }
  }

  public void checkWinsQueueSize(String gameId) {
    Queue<WinRequestDomain> winRequestDomainQueue = wins.get(gameId);
    if (winRequestDomainQueue.size() >= 1000) {
      winRequestDomainQueue.poll();
    }
  }

  public Queue<BetRequestDomain> getBetRequestQueueByGameId(String gameId) {
    return bets.get(gameId);
  }

  public Queue<WinRequestDomain> getWinRequestQueueByGameId(String gameId) {
    return wins.get(gameId);
  }

  public void addGameBet(BetRequestDomain betRequestDomain) {
    Queue<BetRequestDomain> betRequestDomainQueue = bets.get(betRequestDomain.getTransaction().getGameId());
    if (betRequestDomainQueue == null) {
      bets.put(betRequestDomain.getTransaction().getGameId(), new ConcurrentLinkedQueue<>());
      wins.put(betRequestDomain.getTransaction().getGameId(), new ConcurrentLinkedQueue<>());
    }
    bets.get(betRequestDomain.getTransaction().getGameId()).add(betRequestDomain);
  }

  public void addGameWin(WinRequestDomain winRequestDomain) {
    wins.get(winRequestDomain.getTransaction().getGameId()).add(winRequestDomain);
  }

  public boolean isGameExistsInCache(String gameId) {
    return bets.containsKey(gameId);
  }

  public static RequestHandler getInstance() {
    return INSTANCE;
  }


}
