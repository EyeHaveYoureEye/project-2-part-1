package main.java.tech.pm.edu.gameintegrationservice.domain.mapper;


import tech.pm.edu.gameintegrationservice.domain.model.BetTransactionRequestDomain;
import tech.pm.edu.gameintegrationservice.web.model.BetTransactionRequest;

public class BetTransactionRequestMapper {

  private BetTransactionRequestMapper() {
  }

  public static BetTransactionRequestDomain betTransactionRequestToBetTransactionRequestDomain(BetTransactionRequest betTransactionRequest) {
    if (betTransactionRequest == null) {
      return null;
    }
    return new BetTransactionRequestDomain(
      betTransactionRequest.getGameId(),
      betTransactionRequest.getRoundId(),
      betTransactionRequest.getTxId()
    );
  }


}
