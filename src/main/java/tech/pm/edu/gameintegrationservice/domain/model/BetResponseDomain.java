package main.java.tech.pm.edu.gameintegrationservice.domain.model;

import lombok.Value;

@Value
public class BetResponseDomain {

  String status;
  BetDataResponseDomain data;


}
