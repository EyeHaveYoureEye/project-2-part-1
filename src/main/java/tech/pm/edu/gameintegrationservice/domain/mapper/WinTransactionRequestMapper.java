package main.java.tech.pm.edu.gameintegrationservice.domain.mapper;


import tech.pm.edu.gameintegrationservice.domain.model.WinTransactionRequestDomain;
import tech.pm.edu.gameintegrationservice.web.model.WinTransactionRequest;

public class WinTransactionRequestMapper {

  private WinTransactionRequestMapper() {
  }

  public static WinTransactionRequestDomain winTransactionRequestToWinTransactionRequestDomain(WinTransactionRequest winTransactionRequest) {
    if (winTransactionRequest == null) {
      return null;
    }
    return new WinTransactionRequestDomain(
      winTransactionRequest.getGameId(),
      winTransactionRequest.getRoundId(),
      winTransactionRequest.getTxId()
    );
  }


}
