package main.java.tech.pm.edu.gameintegrationservice.domain.service;

import tech.pm.edu.gameintegrationservice.web.model.*;

public interface WinValidationService {

  void gameValidation(WinRequest winRequest);

  void addWinRequest(WinRequest winRequest);


}
