package main.java.tech.pm.edu.gameintegrationservice.domain.model;

import lombok.Value;

import java.math.BigDecimal;

@Value
public class BetDataResponseDomain {

  Integer playerId;
  Integer originalTxId;
  String walletTxId;
  BigDecimal balance;
  String currency;


}
