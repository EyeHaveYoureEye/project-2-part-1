package main.java.tech.pm.edu.gameintegrationservice.domain.model;

import lombok.Value;

@Value
public class WinTransactionRequestDomain {

  String gameId;
  Integer roundId;
  Integer txId;


}
