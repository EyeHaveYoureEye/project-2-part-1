package main.java.tech.pm.edu.gameintegrationservice.domain.model;

import lombok.Value;

@Value
public class BalanceResponseDomain {

  String status;
  BalanceDataResponseDomain balanceDataResponseDomain;


}
