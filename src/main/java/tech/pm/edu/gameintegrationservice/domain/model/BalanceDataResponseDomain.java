package main.java.tech.pm.edu.gameintegrationservice.domain.model;

import lombok.Value;

import java.math.BigDecimal;

@Value
public class BalanceDataResponseDomain {

  Integer playerId;
  BigDecimal balance;
  String currency;


}

