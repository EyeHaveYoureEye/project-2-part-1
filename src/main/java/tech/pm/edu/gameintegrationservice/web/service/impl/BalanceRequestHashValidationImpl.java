package main.java.tech.pm.edu.gameintegrationservice.web.service.impl;


import lombok.Value;
import org.apache.log4j.*;
import tech.pm.edu.gameintegrationservice.web.service.RequestHashValidation;
import tech.pm.edu.gameintegrationservice.web.service.SecretSaltFactory;

import javax.xml.bind.DatatypeConverter;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;


@Value
public class BalanceRequestHashValidationImpl implements RequestHashValidation {

  private static final Logger LOG = Logger.getLogger(BalanceRequestHashValidationImpl.class);

  String playerId;
  String receivedHash;

  private String generateMD5Hash() throws NoSuchAlgorithmException {
    MessageDigest messageDigest = MessageDigest.getInstance("MD5");
    String valueToHash = playerId + SecretSaltFactory.getSecretSaltValue();
    byte[] result = messageDigest.digest(valueToHash.getBytes(StandardCharsets.UTF_8));
    return DatatypeConverter.printHexBinary(result).toLowerCase(Locale.ROOT);
  }

  @Override
  public boolean isHashIdentical() {
    try {
      return generateMD5Hash().equals(receivedHash);
    } catch (NoSuchAlgorithmException e) {
      LOG.error(e);
    }
    return false;
  }


}
