package main.java.tech.pm.edu.gameintegrationservice.web.controller.filter.wrapper;

import javax.servlet.ReadListener;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class RequestWrapper extends HttpServletRequestWrapper {

  private final StringBuffer requestBody = new StringBuffer();

  public RequestWrapper(HttpServletRequest request) throws IOException {
    super(request);
    BufferedReader bufferedReader = request.getReader();
    String line;
    while ((line = bufferedReader.readLine()) != null) {
      requestBody.append(line);
    }
  }

  @Override
  public ServletInputStream getInputStream(){
    final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(requestBody.toString().getBytes());
    return new ServletInputStream() {
      @Override
      public boolean isFinished() {
        return false;
      }

      @Override
      public boolean isReady() {
        return false;
      }

      @Override
      public void setReadListener(ReadListener readListener) {

      }

      public int read(){
        return byteArrayInputStream.read();
      }
    };
  }

  @Override
  public BufferedReader getReader(){
    return new BufferedReader(new InputStreamReader(this.getInputStream()));
  }


}
