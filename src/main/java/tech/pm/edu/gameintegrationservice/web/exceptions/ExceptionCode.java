package main.java.tech.pm.edu.gameintegrationservice.web.exceptions;

public enum ExceptionCode implements ErrorCodeConstant {

  UNEXPECTED_ERROR(CODE_801, "Unexpected error"),
  SECURITY_KEY_MISMATCH(CODE_810, "Security key mismatch"),
  WRONG_PLAYER_ID(CODE_820, "Wrong player Id."),
  BET_ALREADY_SETTLED(CODE_833, "Bet already settled");

  private final int id;
  private final String message;

  ExceptionCode(int id, String message) {
    this.id = id;
    this.message = message;
  }

  public int getId() {
    return this.id;
  }

  public String getMessage() {
    return this.message;
  }


}
