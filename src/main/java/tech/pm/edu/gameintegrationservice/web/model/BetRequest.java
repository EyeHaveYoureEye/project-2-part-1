package main.java.tech.pm.edu.gameintegrationservice.web.model;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class BetRequest {

  private Integer playerId;
  private BigDecimal amount;
  private String currency;
  private BetTransactionRequest transaction;


}
