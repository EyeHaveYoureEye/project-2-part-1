package main.java.tech.pm.edu.gameintegrationservice.web.model.enams;

public enum ResponseStatus {

  SUCCESS, ERRORS;

  @Override
  public String toString() {
    return name().toLowerCase();
  }


}
