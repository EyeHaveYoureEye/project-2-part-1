package main.java.tech.pm.edu.gameintegrationservice.web.context;

import tech.pm.edu.gameintegrationservice.domain.service.*;
import tech.pm.edu.gameintegrationservice.domain.service.impl.*;
import tech.pm.edu.gameintegrationservice.web.api.*;
import tech.pm.edu.gameintegrationservice.web.api.impl.*;
import tech.pm.edu.gameintegrationservice.web.exceptions.service.*;
import tech.pm.edu.gameintegrationservice.web.exceptions.service.impl.*;
import tech.pm.edu.gameintegrationservice.web.model.*;

public class ApplicationContextInjector {

    private static final BetValidationService BET_VALIDATION_SERVICE = new BetValidationServiceImpl();
    private static final WinValidationService WIN_VALIDATION_SERVICE = new WinValidationServiceImpl();

    private static final WalletApiRequest<BetRequest> BET_WALLET_API_REQUEST = new BetWalletApiRequestImpl(BET_VALIDATION_SERVICE);
    private static final WalletApiRequest<String> BALANCE_WALLET_API_REQUEST = new BalanceWalletApiRequestImpl();
    private static final WalletApiRequest<WinRequest> WIN_WALLET_API_REQUEST = new WinWalletApiRequestImpl(WIN_VALIDATION_SERVICE);

    private static final ErrorResponseService ERROR_RESPONSE_SERVICE = new ErrorResponseServiceImpl();

    private ApplicationContextInjector() {}

    public static WalletApiRequest<String> getBalanceWalletApiRequest() {
        return BALANCE_WALLET_API_REQUEST;
    }

    public static WalletApiRequest<WinRequest> getWinWalletApiRequest() {
        return WIN_WALLET_API_REQUEST;
    }

    public static BetValidationService getBetValidationService() {
        return BET_VALIDATION_SERVICE;
    }

    public static ErrorResponseService getErrorResponseService() {
        return ERROR_RESPONSE_SERVICE;
    }

    public static WinValidationService getWinValidationService() {
        return WIN_VALIDATION_SERVICE;
    }

    public static WalletApiRequest<BetRequest> getBetWalletApiRequest() {
        return BET_WALLET_API_REQUEST;
    }

}
