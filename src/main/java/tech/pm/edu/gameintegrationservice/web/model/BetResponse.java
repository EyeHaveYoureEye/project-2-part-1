package main.java.tech.pm.edu.gameintegrationservice.web.model;

import lombok.Data;

@Data
public class BetResponse {

  private String status;
  private BetDataResponse data;


}
