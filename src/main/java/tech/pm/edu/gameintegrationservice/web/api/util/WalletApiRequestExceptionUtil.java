package main.java.tech.pm.edu.gameintegrationservice.web.api.util;

import org.json.JSONException;
import org.json.JSONObject;

public class WalletApiRequestExceptionUtil {

  private WalletApiRequestExceptionUtil() {
  }

  public static boolean isResponseHasError(String jsonData) throws JSONException {
    JSONObject jsonObject = new JSONObject(jsonData);
    return jsonObject.getString("status").equals("errors");
  }


}
