package main.java.tech.pm.edu.gameintegrationservice.web.api.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.log4j.Logger;
import org.json.JSONException;
import tech.pm.edu.gameintegrationservice.domain.service.*;
import tech.pm.edu.gameintegrationservice.web.api.WinWalletApiRequest;
import tech.pm.edu.gameintegrationservice.web.api.util.WalletApiRequestExceptionUtil;
import tech.pm.edu.gameintegrationservice.web.controller.WinServlet;
import tech.pm.edu.gameintegrationservice.web.exceptions.ExceptionCode;
import tech.pm.edu.gameintegrationservice.web.exceptions.WebException;
import tech.pm.edu.gameintegrationservice.web.exceptions.model.ErrorResponseModel;
import tech.pm.edu.gameintegrationservice.web.model.WinRequest;
import tech.pm.edu.gameintegrationservice.web.model.WinResponse;

import java.io.IOException;

public class WinWalletApiRequestImpl extends AbstractGenericWalletApiRequest implements WinWalletApiRequest {
  private static final Logger LOG = Logger.getLogger(WinServlet.class);

  private final WinValidationService winValidationService;
  private final ObjectMapper objectMapper = new ObjectMapper();

  public WinWalletApiRequestImpl(WinValidationService winValidationService) {
    this.winValidationService = winValidationService;
  }

  @Override
  public String getResponseFromApi(WinRequest request) {
    String jsonData;
    WinResponse winResponse;
    String response;
    try {
      jsonData = sendPostRequestToWalletApi("win", objectMapper.writeValueAsString(request));
      if (WalletApiRequestExceptionUtil.isResponseHasError(jsonData)) {
        ErrorResponseModel responseModel = objectMapper.readValue(jsonData, ErrorResponseModel.class);
        return objectMapper.writeValueAsString(responseModel);
      }
      winValidationService.addWinRequest(request);
      winResponse = objectMapper.readValue(jsonData, WinResponse.class);
      response = objectMapper.writeValueAsString(winResponse);
    } catch (JSONException | IOException e) {
      LOG.error("Can't get response from Wallet API, an error occurred", e);
      throw new WebException(ExceptionCode.UNEXPECTED_ERROR);
    }
    return response;
  }


}
