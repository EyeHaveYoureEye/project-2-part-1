package main.java.tech.pm.edu.gameintegrationservice.web.model;

import lombok.Data;

@Data
public class WinTransactionRequest {

  private String gameId;
  private Integer roundId;
  private Integer txId;
  private String walletTxId;


}
