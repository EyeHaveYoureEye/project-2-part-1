package main.java.tech.pm.edu.gameintegrationservice.web.model;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class WinDataResponse {

  private Integer playerId;
  private Integer originalTxId;
  private String walletTxId;
  private BigDecimal balance;
  private String currency;


}
