package main.java.tech.pm.edu.gameintegrationservice.web.model;

import lombok.Data;

@Data
public class WinResponse {

  private String status;
  private WinDataResponse data;


}
