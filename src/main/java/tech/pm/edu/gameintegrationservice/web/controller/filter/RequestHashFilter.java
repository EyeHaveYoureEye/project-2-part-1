package main.java.tech.pm.edu.gameintegrationservice.web.controller.filter;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.log4j.Logger;
import tech.pm.edu.gameintegrationservice.web.context.*;
import tech.pm.edu.gameintegrationservice.web.controller.filter.wrapper.RequestWrapper;
import tech.pm.edu.gameintegrationservice.web.exceptions.ExceptionCode;
import tech.pm.edu.gameintegrationservice.web.exceptions.WebException;
import tech.pm.edu.gameintegrationservice.web.exceptions.model.ErrorResponseModel;
import tech.pm.edu.gameintegrationservice.web.exceptions.service.ErrorResponseService;
import tech.pm.edu.gameintegrationservice.web.exceptions.service.impl.ErrorResponseServiceImpl;
import tech.pm.edu.gameintegrationservice.web.model.HashRequest;
import tech.pm.edu.gameintegrationservice.web.service.RequestHashValidation;
import tech.pm.edu.gameintegrationservice.web.service.impl.BalanceRequestHashValidationImpl;
import tech.pm.edu.gameintegrationservice.web.service.impl.BetAndWinRequestHashValidation;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.stream.Collectors;

@WebFilter("/game/*")
public class RequestHashFilter implements Filter {
  private static final Logger LOG = Logger.getLogger(RequestHashFilter.class);

  private final ObjectMapper objectMapper = new ObjectMapper();
  private final ErrorResponseService errorResponseService = ApplicationContextInjector.getErrorResponseService();

  @Override
  public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
    throws IOException, ServletException {

    RequestWrapper requestWrapper = new RequestWrapper((HttpServletRequest) servletRequest);

    String playerID = requestWrapper.getParameter("playerId");
    String requestHash = requestWrapper.getHeader("request-hash-sign");
    String requestData = requestWrapper.getReader().lines().collect(Collectors.joining(System.lineSeparator()));

    RequestHashValidation requestHashValidation;
    if (requestData.isEmpty()) {
      requestHashValidation = new BalanceRequestHashValidationImpl(playerID, requestHash);
    } else {
      HashRequest hashRequest = objectMapper.readValue(requestData, HashRequest.class);
      requestHashValidation = new BetAndWinRequestHashValidation(hashRequest, requestHash);
    }
    if (requestHash == null || !requestHashValidation.isHashIdentical()) {
      ((HttpServletResponse) servletResponse).setStatus(422);

      ErrorResponseModel errorResponseModel = errorResponseService.getErrorResponseModel(
        new WebException(ExceptionCode.SECURITY_KEY_MISMATCH));
      String responseError = objectMapper.writeValueAsString(errorResponseModel);

      servletResponse.getWriter().write(responseError);
      LOG.error(errorResponseModel.getMessage());
    } else {
      filterChain.doFilter(requestWrapper, servletResponse);
    }
  }


}
