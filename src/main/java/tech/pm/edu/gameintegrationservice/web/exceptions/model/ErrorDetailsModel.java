package main.java.tech.pm.edu.gameintegrationservice.web.exceptions.model;

import lombok.Data;

@Data
public class ErrorDetailsModel {

  private int code;
  private String message;


}
