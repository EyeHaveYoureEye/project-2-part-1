package main.java.tech.pm.edu.gameintegrationservice.web.api.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.log4j.Logger;
import org.json.JSONException;
import tech.pm.edu.gameintegrationservice.domain.service.BetValidationService;
import tech.pm.edu.gameintegrationservice.domain.service.impl.BetValidationServiceImpl;
import tech.pm.edu.gameintegrationservice.web.api.BetWalletApiRequest;
import tech.pm.edu.gameintegrationservice.web.api.util.WalletApiRequestExceptionUtil;
import tech.pm.edu.gameintegrationservice.web.exceptions.ExceptionCode;
import tech.pm.edu.gameintegrationservice.web.exceptions.WebException;
import tech.pm.edu.gameintegrationservice.web.exceptions.model.ErrorResponseModel;
import tech.pm.edu.gameintegrationservice.web.model.BetRequest;
import tech.pm.edu.gameintegrationservice.web.model.BetResponse;

import java.io.IOException;

public class BetWalletApiRequestImpl extends AbstractGenericWalletApiRequest implements BetWalletApiRequest {
  private static final Logger LOG = Logger.getLogger(BetWalletApiRequestImpl.class);

  private final ObjectMapper objectMapper = new ObjectMapper();
  private final BetValidationService betValidationService;

  public BetWalletApiRequestImpl(BetValidationService betValidationService) {
    this.betValidationService = betValidationService;
  }

  @Override
  public String getResponseFromApi(BetRequest request) {
    String jsonData;
    BetResponse betResponse;
    String response;
    try {
      jsonData = sendPostRequestToWalletApi("bet", objectMapper.writeValueAsString(request));
      if (WalletApiRequestExceptionUtil.isResponseHasError(jsonData)) {
        ErrorResponseModel responseModel = objectMapper.readValue(jsonData, ErrorResponseModel.class);
        return objectMapper.writeValueAsString(responseModel);
      }
      betValidationService.addGameBet(request);
      betResponse = objectMapper.readValue(jsonData, BetResponse.class);
      response = objectMapper.writeValueAsString(betResponse);
    } catch (JSONException | IOException e) {
      LOG.error("An error by getting response from wallet occurred: " + ExceptionCode.UNEXPECTED_ERROR);
      throw new WebException(ExceptionCode.UNEXPECTED_ERROR);
    }
    return response;
  }


}
