package main.java.tech.pm.edu.gameintegrationservice.web.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class BalanceResponse {

  private String status;
  @JsonProperty("data")
  private BalanceDataResponse balanceDataResponse;


}
