package main.java.tech.pm.edu.gameintegrationservice.web.service.impl;

import lombok.Value;
import org.apache.log4j.Logger;
import tech.pm.edu.gameintegrationservice.web.model.HashRequest;
import tech.pm.edu.gameintegrationservice.web.model.HashRequestData;
import tech.pm.edu.gameintegrationservice.web.service.RequestHashValidation;
import tech.pm.edu.gameintegrationservice.web.service.SecretSaltFactory;

import javax.xml.bind.DatatypeConverter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;

import static java.util.stream.Collectors.toList;

@Value
public class BetAndWinRequestHashValidation implements RequestHashValidation {

  private static final Logger LOG = Logger.getLogger(BetAndWinRequestHashValidation.class);

  HashRequest request;
  String receivedHash;

  private String generateMD5Hash() throws NoSuchAlgorithmException {

    MessageDigest messageDigest = MessageDigest.getInstance("MD5");
    byte[] result = new byte[0];

    try {
      result = messageDigest.digest(getConceitedFields().getBytes(StandardCharsets.UTF_8));
    } catch (IllegalAccessException | InvocationTargetException e) {
      LOG.error(e);
    }
    return DatatypeConverter.printHexBinary(result).toLowerCase(Locale.ROOT);
  }

  @Override
  public boolean isHashIdentical() {
    try {
      return generateMD5Hash().equals(receivedHash);
    } catch (NoSuchAlgorithmException e) {
      LOG.error(e);
    }
    return false;
  }

  private String getConceitedFields() throws IllegalAccessException, InvocationTargetException {

    List<String> fields = getSortedFieldsValues();
    StringBuilder stringBuilder = new StringBuilder();

    for (String value : fields) {
      stringBuilder.append(value);
    }
    stringBuilder.append(SecretSaltFactory.getSecretSaltValue());
    return stringBuilder.toString();
  }

  private <E> Map<String, String> getMapFromEntity(E entity) throws IllegalAccessException, InvocationTargetException {

    Method[] methods = entity.getClass().getMethods();
    Map<String, String> fieldsMap = new HashMap<>();

    for (Method method : methods) {
      if (method.getName().startsWith("get") && !method.getName().equals("getClass")) {
        if (method.invoke(entity).getClass().equals(HashRequestData.class)) {
          HashRequestData newEntity = (HashRequestData) method.invoke(entity);
          fieldsMap.putAll(getMapFromEntity(newEntity));
        } else {
          String value = method.invoke(entity).toString();
          fieldsMap.put(method.getName().substring(3), value);
        }

      }
    }
    return fieldsMap;
  }


  private List<String> getSortedFieldsValues() throws InvocationTargetException, IllegalAccessException {

    Map<String, String> fieldsMap = getMapFromEntity(request);

    return fieldsMap.keySet()
            .stream()
            .sorted()
            .map(fieldsMap::get)
            .collect(toList());
  }


}


