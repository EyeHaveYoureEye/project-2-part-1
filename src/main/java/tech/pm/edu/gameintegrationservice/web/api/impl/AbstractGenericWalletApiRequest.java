package main.java.tech.pm.edu.gameintegrationservice.web.api.impl;

import org.apache.log4j.*;
import tech.pm.edu.gameintegrationservice.domain.service.impl.*;
import tech.pm.edu.gameintegrationservice.web.api.connection.ConnectionFactory;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.nio.charset.StandardCharsets;

public abstract class AbstractGenericWalletApiRequest {
  private static final Logger LOG = Logger.getLogger(WinValidationServiceImpl.class);

  public String sendGetRequestToWalletApi(String path) throws IOException {
    HttpURLConnection connection = null;
    try {
      connection = ConnectionFactory.getConnection(path);
      connection.setRequestMethod("GET");
      connection.setRequestProperty("Accept", "application/json");
      connection.setDoOutput(true);
      return getResponseFromWalletApi(connection);
    } catch (IOException ex) {
      LOG.error(ex);
      throw new IOException();
    } finally {
      if (connection != null) {
        try {
          connection.disconnect();
        } catch (Exception ex) {
          LOG.error(ex);
        }
      }
    }
  }

  public String sendPostRequestToWalletApi(String path, String requestData) throws IOException {
    HttpURLConnection connection = null;
    try {
      connection = ConnectionFactory.getConnection(path);
      connection.setRequestMethod("POST");
      connection.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
      connection.setRequestProperty("Accept", "application/json");
      connection.setDoOutput(true);
      connection.connect();
      try (DataOutputStream wr = new DataOutputStream(connection.getOutputStream())) {
        byte[] postData = requestData.getBytes(StandardCharsets.UTF_8);
        wr.write(postData);
      }
      return getResponseFromWalletApi(connection);
    } catch (IOException ex) {
      LOG.error(ex);
      throw new IOException();
    } finally {
      if (connection != null) {
        try {
          connection.disconnect();
        } catch (Exception ex) {
          LOG.error(ex);
        }
      }
    }
  }

  private String getResponseFromWalletApi(HttpURLConnection connection) throws IOException {
    BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
    StringBuilder sb = new StringBuilder();
    String line;
    while ((line = br.readLine()) != null) {
      sb.append(line).append("\n");
    }
    br.close();
    return sb.toString();
  }


}
