package main.java.tech.pm.edu.gameintegrationservice.web.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import tech.pm.edu.gameintegrationservice.domain.service.WinValidationService;
import tech.pm.edu.gameintegrationservice.web.api.WalletApiRequest;
import tech.pm.edu.gameintegrationservice.web.api.util.WalletApiRequestExceptionUtil;
import tech.pm.edu.gameintegrationservice.web.context.*;
import tech.pm.edu.gameintegrationservice.web.exceptions.WebException;
import tech.pm.edu.gameintegrationservice.web.exceptions.model.ErrorResponseModel;
import tech.pm.edu.gameintegrationservice.web.exceptions.service.ErrorResponseService;
import tech.pm.edu.gameintegrationservice.web.model.WinRequest;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.stream.Collectors;

@WebServlet("/game/win")
public class WinServlet extends HttpServlet {
  private static final Logger LOG = Logger.getLogger(WinServlet.class);

  private ObjectMapper objectMapper = new ObjectMapper();
  private WalletApiRequest<WinRequest> walletApiWinRequest;
  private WinValidationService winValidationService;
  private ErrorResponseService errorResponseService;

  @Override
  public void init() {
    objectMapper = new ObjectMapper();
    walletApiWinRequest = ApplicationContextInjector.getWinWalletApiRequest();
    winValidationService = ApplicationContextInjector.getWinValidationService();
    errorResponseService = ApplicationContextInjector.getErrorResponseService();
  }

  @SneakyThrows
  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
    String requestData = req.getReader().lines().collect(Collectors.joining(System.lineSeparator()));

    JSONObject json = new JSONObject(requestData);
    JSONObject transactionJson = new JSONObject(json.getString("transaction"));

    transactionJson.put("walletTxId", json.hashCode());
    json.put("transaction", transactionJson);

    WinRequest winRequest = objectMapper.readValue(json.toString(), WinRequest.class);
    String response;
    try {
      winValidationService.gameValidation(winRequest);
      response = walletApiWinRequest.getResponseFromApi(winRequest);
      if (WalletApiRequestExceptionUtil.isResponseHasError(response)) {
        resp.setStatus(422);
      }
      resp.getWriter().write(response);
    } catch (WebException webException) {
      ErrorResponseModel errorResponseModel = errorResponseService.getErrorResponseModel(webException);
      response = objectMapper.writeValueAsString(errorResponseModel);

      resp.setStatus(422);
      resp.getWriter().write(response);
    }
  }


}
