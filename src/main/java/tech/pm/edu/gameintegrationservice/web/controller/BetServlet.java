package main.java.tech.pm.edu.gameintegrationservice.web.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import tech.pm.edu.gameintegrationservice.domain.service.BetValidationService;
import tech.pm.edu.gameintegrationservice.web.api.WalletApiRequest;
import tech.pm.edu.gameintegrationservice.web.api.util.WalletApiRequestExceptionUtil;
import tech.pm.edu.gameintegrationservice.web.context.*;
import tech.pm.edu.gameintegrationservice.web.exceptions.WebException;
import tech.pm.edu.gameintegrationservice.web.exceptions.model.ErrorResponseModel;
import tech.pm.edu.gameintegrationservice.web.exceptions.service.ErrorResponseService;
import tech.pm.edu.gameintegrationservice.web.model.BetRequest;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.stream.Collectors;

@WebServlet("/game/bet")
public class BetServlet extends HttpServlet {

  private static final Logger LOG = Logger.getLogger(BetServlet.class);
  private ObjectMapper objectMapper;
  private WalletApiRequest<BetRequest> walletApiBetRequest;
  private BetValidationService betValidationService;
  private ErrorResponseService errorResponseService;

  @Override
  public void init() {
    objectMapper = new ObjectMapper();
    walletApiBetRequest = ApplicationContextInjector.getBetWalletApiRequest();
    betValidationService = ApplicationContextInjector.getBetValidationService();
    errorResponseService = ApplicationContextInjector.getErrorResponseService();
  }

  @SneakyThrows
  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
    String requestData = req.getReader().lines().collect(Collectors.joining(System.lineSeparator()));

    JSONObject json = new JSONObject(requestData);
    JSONObject transactionJson = new JSONObject(json.getString("transaction"));

    transactionJson.put("walletTxId", json.hashCode());
    json.put("transaction", transactionJson);

    BetRequest betRequest = objectMapper.readValue(json.toString(), BetRequest.class);
    String response;
    try {
      betValidationService.gameValidation(betRequest);
      response = walletApiBetRequest.getResponseFromApi(betRequest);
      if (WalletApiRequestExceptionUtil.isResponseHasError(response)) {
        resp.setStatus(422);
      }
      resp.getWriter().write(response);
    } catch (WebException webException) {
      ErrorResponseModel errorResponseModel = errorResponseService.getErrorResponseModel(webException);
      response = objectMapper.writeValueAsString(errorResponseModel);

      resp.setStatus(422);
      resp.getWriter().write(response);
    }
  }


}
