package main.java.tech.pm.edu.gameintegrationservice.web.api;

public interface WalletApiRequest<R> {

  String getResponseFromApi(R request);


}
