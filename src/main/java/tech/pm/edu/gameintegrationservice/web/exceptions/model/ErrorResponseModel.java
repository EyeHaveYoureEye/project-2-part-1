package main.java.tech.pm.edu.gameintegrationservice.web.exceptions.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.LinkedList;
import java.util.List;

@Data
public class ErrorResponseModel {

  private String status;
  private String message;
  @JsonProperty("errors")
  private List<ErrorDetailsModel> errors = new LinkedList<>();

  public void addErrorDetailsToErrorsList(ErrorDetailsModel errorDetailsModel) {
    errors.add(errorDetailsModel);
  }


}
