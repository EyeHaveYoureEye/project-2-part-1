package main.java.tech.pm.edu.gameintegrationservice.web.model;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class BalanceDataResponse {

  private Integer playerId;
  private BigDecimal balance;
  private String currency;


}

