package main.java.tech.pm.edu.gameintegrationservice.web.model;

import lombok.Data;

@Data
public class HashRequest {

  private Integer playerId;
  private String amount;
  private String currency;
  private HashRequestData transaction;


}

