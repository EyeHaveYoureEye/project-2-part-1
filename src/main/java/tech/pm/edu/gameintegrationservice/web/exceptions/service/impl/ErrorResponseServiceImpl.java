package main.java.tech.pm.edu.gameintegrationservice.web.exceptions.service.impl;

import tech.pm.edu.gameintegrationservice.web.exceptions.WebException;
import tech.pm.edu.gameintegrationservice.web.exceptions.model.ErrorDetailsModel;
import tech.pm.edu.gameintegrationservice.web.exceptions.model.ErrorResponseModel;
import tech.pm.edu.gameintegrationservice.web.exceptions.service.ErrorResponseService;
import tech.pm.edu.gameintegrationservice.web.model.enams.ResponseStatus;

public class ErrorResponseServiceImpl implements ErrorResponseService {

  @Override
  public ErrorResponseModel getErrorResponseModel(WebException webException) {
    return webExceptionToErrorResponseModel(webException);
  }

  private ErrorResponseModel webExceptionToErrorResponseModel(WebException exception) {
    ErrorDetailsModel errorDetailsModel = new ErrorDetailsModel();
    errorDetailsModel.setCode(exception.getErrorCode());
    errorDetailsModel.setMessage(exception.getErrorMessage());
    ErrorResponseModel errorResponseModel = new ErrorResponseModel();
    errorResponseModel.setStatus(ResponseStatus.ERRORS.toString());
    errorResponseModel.setMessage(exception.getErrorMessage());
    errorResponseModel.addErrorDetailsToErrorsList(errorDetailsModel);
    return errorResponseModel;
  }


}
