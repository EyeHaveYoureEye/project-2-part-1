package main.java.tech.pm.edu.gameintegrationservice.web.api;

import tech.pm.edu.gameintegrationservice.web.model.BetRequest;

public interface BetWalletApiRequest extends WalletApiRequest<BetRequest> {


}
