package main.java.tech.pm.edu.gameintegrationservice.web.exceptions.service;

import tech.pm.edu.gameintegrationservice.web.exceptions.WebException;
import tech.pm.edu.gameintegrationservice.web.exceptions.model.ErrorResponseModel;

public interface ErrorResponseService {

  ErrorResponseModel getErrorResponseModel(WebException webException);


}
