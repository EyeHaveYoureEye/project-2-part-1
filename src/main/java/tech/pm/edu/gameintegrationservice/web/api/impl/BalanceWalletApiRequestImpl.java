package main.java.tech.pm.edu.gameintegrationservice.web.api.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.log4j.Logger;
import org.json.JSONException;
import tech.pm.edu.gameintegrationservice.web.api.BalanceWalletApiRequest;
import tech.pm.edu.gameintegrationservice.web.api.util.WalletApiRequestExceptionUtil;
import tech.pm.edu.gameintegrationservice.web.exceptions.ExceptionCode;
import tech.pm.edu.gameintegrationservice.web.exceptions.WebException;
import tech.pm.edu.gameintegrationservice.web.exceptions.model.ErrorResponseModel;
import tech.pm.edu.gameintegrationservice.web.model.BalanceResponse;

import java.io.IOException;
import java.net.MalformedURLException;

public class BalanceWalletApiRequestImpl extends AbstractGenericWalletApiRequest implements BalanceWalletApiRequest {

  ObjectMapper objectMapper = new ObjectMapper();
  private static final Logger LOG = Logger.getLogger(BalanceWalletApiRequestImpl.class);

  @Override
  public String getResponseFromApi(String request) {
    String jsonData;
    BalanceResponse balanceResponse;
    String response;
    try {
      jsonData = sendGetRequestToWalletApi("balance?playerId=" + request);
      if (WalletApiRequestExceptionUtil.isResponseHasError(jsonData)) {
        ErrorResponseModel responseModel = objectMapper.readValue(jsonData, ErrorResponseModel.class);
        return objectMapper.writeValueAsString(responseModel);
      }
      balanceResponse = objectMapper.readValue(jsonData, BalanceResponse.class);
      response = objectMapper.writeValueAsString(balanceResponse);
    } catch (MalformedURLException e) {
      LOG.error("Wrong player ID exception");
      throw new WebException(ExceptionCode.WRONG_PLAYER_ID);
    } catch (JSONException | IOException e) {
      LOG.error("Unexpected Error", e);
      throw new WebException(ExceptionCode.UNEXPECTED_ERROR);
    }
    return response;
  }


}
