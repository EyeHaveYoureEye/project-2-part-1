package main.java.tech.pm.edu.gameintegrationservice.web.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.log4j.Logger;
import tech.pm.edu.gameintegrationservice.web.api.WalletApiRequest;
import tech.pm.edu.gameintegrationservice.web.context.*;
import tech.pm.edu.gameintegrationservice.web.exceptions.WebException;
import tech.pm.edu.gameintegrationservice.web.exceptions.model.ErrorResponseModel;
import tech.pm.edu.gameintegrationservice.web.exceptions.service.ErrorResponseService;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/game/balance")
public class BalanceServlet extends HttpServlet {

  private static final Logger LOG = Logger.getLogger(BalanceServlet.class);
  private ObjectMapper objectMapper;
  private WalletApiRequest<String> walletApiBalanceRequest;
  private ErrorResponseService errorResponseService;

  @Override
  public void init() {
    objectMapper = new ObjectMapper();
    walletApiBalanceRequest = ApplicationContextInjector.getBalanceWalletApiRequest();
    errorResponseService = ApplicationContextInjector.getErrorResponseService();
  }

  @Override
  public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
    resp.setContentType("application/json;charset=UTF-8");

    String playerId = req.getParameter("playerId");
    String response;
    try {
      response = walletApiBalanceRequest.getResponseFromApi(playerId);

      resp.getWriter().write(response);
    } catch (WebException webException) {
      ErrorResponseModel errorResponseModel = errorResponseService.getErrorResponseModel(webException);
      response = objectMapper.writeValueAsString(errorResponseModel);

      resp.getWriter().write(response);
      LOG.error("Getting balance exception: " + webException.getErrorMessage());
    }
  }


}
