package main.java.tech.pm.edu.gameintegrationservice.web.api.connection;

import org.apache.log4j.Logger;
import tech.pm.edu.gameintegrationservice.web.exceptions.*;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ResourceBundle;

public class ConnectionFactory {

  private static final ConnectionFactory INSTANCE = new ConnectionFactory();
  private static ResourceBundle apiConfig;
  private static final String URL_CONFIG = apiConfig.getString("api.url");
  private static final Logger LOG = Logger.getLogger(ConnectionFactory.class);


  private ConnectionFactory() {
    try {
      apiConfig = ResourceBundle.getBundle("api");
    } catch (Exception e) {
      LOG.error(e);
    }
  }

  private HttpURLConnection createConnection(String path) throws MalformedURLException {
    String connectionUrl = URL_CONFIG + path;
    URL url = new URL(connectionUrl);
    try {
      return (HttpURLConnection) url.openConnection();
    } catch (Exception e) {
      LOG.error(e);
      throw new WebException(ExceptionCode.UNEXPECTED_ERROR);
    }
  }

  public static HttpURLConnection getConnection(String path) throws MalformedURLException {
    return INSTANCE.createConnection(path);
  }


}
